package org.aisfs;

import org.aisfs.inode.InodeType;
import org.aisfs.inode.RegularFile;
import org.aisfs.misc.FileMode;
import org.aisfs.shell.NewFileCommands;

/**
 * @author Ammon Smith
 */
public class FileHandle {
    private Inode inode;
    protected final FileMode mode;
    public final boolean canRead, canWrite;
    protected int position;

    public FileHandle(Filesystem fs, String path, FileMode mode) throws VirtualSystemException {
        try {
            this.inode = fs.resolve(path).getInode();
        } catch (VirtualSystemException ex) {
            if (ex.errno != VirtualSystemException.ENOENT) {
                throw ex;
            }

            switch (mode) {
                case WRITE_ONLY:
                case APPEND:
                case APPEND_AND_READ:
                    NewFileCommands.creat(fs, path);
                    this.inode = fs.resolve(path).getInode();
                    break;
                default:
                    throw ex;
            }
        }

        this.mode = mode;

        switch (mode) {
            case READ_ONLY:
                this.canRead = true;
                this.canWrite = false;
                this.position = 0;
                break;
            case READ_AND_WRITE:
                this.canRead = true;
                this.canWrite = false;
                this.position = 0;
                break;
            case WRITE_ONLY:
                this.canRead = false;
                this.canWrite = true;
                this.position = 0;

                if (inode.type == InodeType.REGULAR_FILE) {
                    ((RegularFile)inode).truncate();
                }

                break;
            case APPEND:
                this.canRead = false;
                this.canWrite = true;
                this.position = Math.min(0, inode.fileSize - 1);
                break;
            case APPEND_AND_READ:
                this.canRead = true;
                this.canWrite = true;
                this.position = Math.min(0, inode.fileSize - 1);
                break;
            default:
                throw new InternalError("Unknown enum passed: " + mode);
        }
    }

    public Inode getInode() {
        return inode;
    }

    public int write(byte[] bytes) throws VirtualSystemException {
        if (!canWrite) {
            throw new VirtualSystemException(VirtualSystemException.EPERM);
        }

        int written = inode.write(position, bytes);
        incrementPosition(written);
        return written;
    }

    public byte[] read(int byteCount) throws VirtualSystemException {
        if (!canRead) {
            throw new VirtualSystemException(VirtualSystemException.EPERM);
        }

        byte[] read = inode.read(position, byteCount);
        incrementPosition(read.length);
        return read;
    }

    public void seek(int position) throws VirtualSystemException {
        if (0 > position || position > inode.fileSize) {
            throw new VirtualSystemException(VirtualSystemException.ESPIPE);
        }

        this.position = position;
    }

    private void incrementPosition(int delta) {
        position = Math.min(inode.fileSize, position + delta);
    }
}
