package org.aisfs.misc;

/**
 * @author Ammon Smith
 */
public enum FileMode {
    READ_ONLY,
    READ_AND_WRITE,
    WRITE_ONLY,
    APPEND,
    APPEND_AND_READ,
}
