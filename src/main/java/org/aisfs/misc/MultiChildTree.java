package org.aisfs.misc;

import com.sun.istack.internal.NotNull;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Ammon Smith
 */
public class MultiChildTree<K, V> {
    private V value;
    private final Map<K, MultiChildNode> children;

    public MultiChildTree() {
        value = null;
        children = new HashMap<>();
    }

    public void addPair(@NotNull K[] keyPath, V value) {
        if (keyPath.length == 0) {
            this.value = value;
            return;
        }

        MultiChildNode node = children.get(keyPath[0]);
        for (int i = 1; i < keyPath.length; i++) {
            node = node.getNode(keyPath[i]);
        }

        node.setValue(value);
    }

    public V getValue(@NotNull K[] keyPath) {
        if (keyPath.length == 0) {
            return value;
        }

        MultiChildNode node = children.get(keyPath[0]);
        for (int i = 1; i < keyPath.length; i++) {
            node = node.getNode(keyPath[i]);
        }

        return node.getValue();
    }

    private class MultiChildNode {
        private V value;
        private final Map<K, MultiChildNode> children;

        MultiChildNode(V value) {
            this.value = value;
            this.children = new HashMap<>();
        }

        MultiChildNode() {
            this(null);
        }

        public V getValue() {
            return value;
        }

        public void setValue(V value) {
            this.value = value;
        }

        public MultiChildNode getNode(K key) {
            if (children.containsKey(key)) {
                return children.get(key);
            } else {
                MultiChildNode node = new MultiChildNode();
                children.put(key, node);
                return node;
            }
        }
    }
}
