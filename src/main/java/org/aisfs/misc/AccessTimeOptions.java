package org.aisfs.misc;

/**
 * @author Ammon Smith
 */
public enum AccessTimeOptions {
    NORMAL_ACCESS_TIME,
    RELATIVE_ACCESS_TIME,
    NO_ACCESS_TIME,
}
