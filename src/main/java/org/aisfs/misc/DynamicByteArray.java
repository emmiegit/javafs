package org.aisfs.misc;

import org.aisfs.VirtualSystemException;

/**
 * @author Ammon Smith
 */
public class DynamicByteArray {
    public static final int DEFAULT_CAPACITY = 512;

    private byte[] bytes;
    private int size;

    public DynamicByteArray(byte[] data) {
        bytes = new byte[data.length];
        System.arraycopy(data, 0, bytes, 0, data.length);
    }

    public DynamicByteArray(int capacity) {
        bytes = new byte[capacity];
    }

    public DynamicByteArray() {
        this(DEFAULT_CAPACITY);
    }

    public int getSize() {
        return size;
    }

    public byte get(int index) {
        checkIndex(index);
        return bytes[index];
    }

    public byte[] get(int index, int length) {
        if (length == 0) {
            return new byte[0];
        }

        length = Math.min(length, bytes.length - index);
        checkIndex(index + length - 1);
        byte[] copy = new byte[length];
        System.arraycopy(bytes, index, copy, 0, length);
        return copy;
    }

    public void set(int index, byte value) {
        checkIndex(index);
        bytes[index] = value;
    }

    public void set(int index, byte[] data) throws VirtualSystemException {
        if (index + data.length >= bytes.length) {
            ensureSpace(data.length);
        }

        System.arraycopy(data, 0, bytes, index, data.length);
        size += data.length;
    }

    public void append(byte value) throws VirtualSystemException {
        ensureSpace(1);
        bytes[size++] = value;
    }

    public void append(byte[] data) throws VirtualSystemException {
        ensureSpace(data.length);
        System.arraycopy(data, 0, bytes, size, data.length);
        size += data.length;
    }

    public void clear() {
        bytes = new byte[0];
    }

    private void checkIndex(int index) {
        if (0 > index || index >= size) {
            throw new IndexOutOfBoundsException(String.format("Index %d not in range: 0 - %d.", index, size));
        }
    }

    private void ensureSpace(int length) throws VirtualSystemException {
        final int capacity = bytes.length - size + length;

        if (capacity < 0) {
            throw new VirtualSystemException(VirtualSystemException.ENOSPC);
        } else if (capacity > bytes.length) {
            byte[] newBytes = new byte[capacity];
            System.arraycopy(bytes, 0, newBytes, 0, size);
            bytes = newBytes;
        }
    }
}
