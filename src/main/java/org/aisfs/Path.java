package org.aisfs;

import com.sun.istack.internal.NotNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * @author Ammon Smith
 */
public class Path implements Iterable<String> {
    public final boolean absolute;
    private final List<String> parts;

    public Path(@NotNull String path) {
        parts = new ArrayList<>();
        absolute = path.startsWith(Filesystem.PATH_SEPARATOR);

        for (String part : path.split(Filesystem.PATH_SEPARATOR)) {
            if (part.isEmpty()) {
                continue;
            }

            for (char ch : part.toCharArray()) {
                if (ch == '\0') {
                    throw new IllegalArgumentException("Paths may not contain null characters ('\\0').");
                }
            }

            parts.add(part);
        }

        if (parts.isEmpty() && !absolute) {
            throw new IllegalArgumentException("Paths may not be empty.");
        }
    }

    @NotNull
    public List<String> getParts() {
        return Collections.unmodifiableList(parts);
    }

    @NotNull
    public String[] getPartArray() {
        return parts.toArray(new String[parts.size()]);
    }

    public int getPartCount() {
        return parts.size();
    }

    @Override
    public Iterator<String> iterator() {
        return parts.iterator();
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder(Filesystem.PATH_SEPARATOR);

        for (int i = 0; i < parts.size(); i++) {
            str.append(parts.get(i));

            if (i < parts.size() - 1) {
                str.append(Filesystem.PATH_SEPARATOR);
            }
        }

        return str.toString();
    }
}
