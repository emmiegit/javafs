package org.aisfs.shell;

import org.aisfs.Filesystem;
import org.aisfs.VirtualSystemException;
import org.aisfs.Inode;
import org.aisfs.filesystem.DeviceFilesystem;
import org.aisfs.filesystem.SubFilesystem;
import org.aisfs.inode.Directory;
import org.aisfs.inode.InodeType;
import org.aisfs.inode.SymbolicLink;
import org.aisfs.inode.directory.DirectoryEntry;
import org.aisfs.system.Kernel;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Ammon Smith
 */
public final class FilesystemCommands {
    private FilesystemCommands() {
    }

    public static void chgrp(Filesystem fs, String[] args) {
        if (args.length < 3) {
            System.out.println("Usage: chgrp group file...");
            return;
        }

        short gid;

        try {
            gid = Short.parseShort(args[1]);
        } catch (NumberFormatException ex) {
            System.out.printf("chgrp: not a number or out of range: %s.\n", args[1]);
            return;
        }

        for (int i = 2; i < args.length; i++) {
            chgrp(fs, args[i], gid);
        }
    }

    private static void chgrp(Filesystem fs, String path, short gid) {
        try {
            DirectoryEntry<?> dirent = fs.resolve(path);
            dirent.getInode().setOwner(dirent.getInode().getGroupID(), gid);
        } catch (IllegalArgumentException ex) {
            System.out.printf("chgrp: invalid user id: %d.\n", gid);
        } catch (VirtualSystemException ex) {
            System.out.printf("chgrp: %s: %s\n", path, ex.getMessage());
        }
    }


    public static void chmod(Filesystem fs, String[] args) {
        if (args.length < 3) {
            System.out.println("Usage: chmod mode file...");
            return;
        }

        short mode;

        try {
            mode = (short)Integer.parseInt(args[1], 8);
        } catch (NumberFormatException ex) {
            System.out.printf("chmod: not a number or out of range: %s.\n", args[1]);
            return;
        }

        for (int i = 2; i < args.length; i++) {
            chmod(fs, args[i], mode);
        }
    }

    private static void chmod(Filesystem fs, String path, short mode) {
        try {
            DirectoryEntry<?> dirent = fs.resolve(path);
            dirent.getInode().setMode(mode);
        } catch (IllegalArgumentException ex) {
            System.out.printf("chmod: invalid mode: %s.\n", Integer.toOctalString(mode));
        } catch (VirtualSystemException ex) {
            System.out.printf("chmod: %s: %s\n", path, ex.getMessage());
        }
    }

    public static void chown(Filesystem fs, String[] args) {
        if (args.length < 3) {
            System.out.println("Usage:");
            System.out.println("chown user:group file...");
            System.out.println("chown user file...");
            return;
        }

        boolean specifiesGroup;
        short uid, gid;

        try {
            if (args[1].contains(":")) {
                String[] parts = args[1].split(":");

                if (parts.length != 2) {
                    System.out.println("Usage:");
                    System.out.println("chown user:group file...");
                    System.out.println("chown user file...");
                    return;
                }

                uid = Short.parseShort(parts[0]);
                gid = Short.parseShort(parts[1]);
                specifiesGroup = true;
            } else {
                uid = (short)Integer.parseInt(args[1]);
                gid = -1;
                specifiesGroup = false;
            }

        } catch (NumberFormatException ex) {
            System.out.printf("chown: not a number or out of range: %s.\n", args[1]);
            return;
        }

        if (specifiesGroup) {
            for (int i = 2; i < args.length; i++) {
                chown(fs, args[i], uid, gid);
            }
        } else {
            for (int i = 2; i < args.length; i++) {
                chown(fs, args[i], uid);
            }
        }
    }

    private static void chown(Filesystem fs, String path, short uid, short gid) {
        try {
            DirectoryEntry<?> dirent = fs.resolve(path);

            dirent.getInode().setOwner(uid, gid);
        } catch (IllegalArgumentException ex) {
            System.out.printf("chown: invalid user id: %d:%d.\n", uid, gid);
        } catch (VirtualSystemException ex) {
            System.out.printf("chown: %s: %s\n", path, ex.getMessage());
        }
    }

    private static void chown(Filesystem fs, String path, short uid) {
        try {
            DirectoryEntry<?> dirent = fs.resolve(path);

            dirent.getInode().setOwner(uid, dirent.getInode().getGroupID());
        } catch (IllegalArgumentException ex) {
            System.out.printf("chown: invalid user id: %d.\n", uid);
        } catch (VirtualSystemException ex) {
            System.out.printf("chown: %s: %s\n", path, ex.getMessage());
        }
    }
    public static void ls(Filesystem fs, String[] args) {
        if (args.length == 1) {
            System.out.println("Usage: ls directory...");
            return;
        }

        for (int i = 1; i < args.length; i++) {
            lsFile(fs, args[i]);
        }
    }

    private static void lsFile(Filesystem fs, String path) {
        try {
            DirectoryEntry<?> dirent = fs.resolve(path);
            if (dirent.getInode().type != InodeType.DIRECTORY) {
                System.out.println(dirent.getName());
            } else {
                for (Object entry : dirent.getInode().getLinks()) {
                    System.out.print(((DirectoryEntry<?>)entry).getName());
                    System.out.print(" ");
                }

                System.out.println();
            }
        } catch (VirtualSystemException ex) {
            System.out.printf("ls: %s: %s\n", path, ex.getMessage());
        }
    }

    public static void ll(Filesystem fs, String[] args) {
        if (args.length == 1) {
            System.out.println("Usage: ll directory...");
            return;
        }

        System.out.println("ID   Mode       Links  UID   GID  Size   Last Modified      Name");

        for (int i = 1; i < args.length; i++) {
            llFile(fs, args[i], args.length == 2);
        }
    }

    private static final SimpleDateFormat llDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

    private static void llFile(Filesystem fs, String path, boolean printDir) {
        try {
            DirectoryEntry<?> dirent = fs.resolve(path);
            if (dirent.getInode().type != InodeType.DIRECTORY) {
                System.out.println(dirent.getName());
            } else {
                if (printDir) {
                    System.out.printf("%s:\n", path);
                }

                for (Object item : dirent.getInode().getLinks()) {
                    DirectoryEntry<?> entry = (DirectoryEntry<?>)item;
                    StringBuilder line = new StringBuilder();

                    // Inode ID
                    line.append(String.format("%-4d", entry.getInode().getDeviceId()))
                        .append(' ');

                    // Permissions
                    line.append(llMode(entry.getInode()))
                        .append(' ');

                    // Links
                    line.append(String.format("%-6d", entry.getInode().getLinkCount()))
                        .append(' ');

                    // Owner
                    line.append(String.format("%-5s", Kernel.getInstance().userManager.safeGetUserName(entry.getInode().getUserID())))
                        .append(' ');

                    // Group
                    line.append(String.format("%-5d", Kernel.getInstance().userManager.safeGetGroupName(entry.getInode().getGroupID())))
                        .append(' ');

                    // Size
                    line.append(String.format("%-5d", entry.getInode().getFileSize()))
                        .append(' ');

                    // Access date
                    line.append(String.format("%-8s", llDateFormat.format(new Date(entry.getInode().getAccessTime()))))
                        .append(' ');

                    // Name
                    line.append(entry.getName());

                    // Symbol
                    switch (entry.getInode().type) {
                        case REGULAR_FILE:
                        case BLOCK_DEVICE:
                        case CHARACTER_DEVICE:
                            break;
                        case DIRECTORY:
                            line.append(Filesystem.PATH_SEPARATOR);
                            break;
                        case SYMBOLIC_LINK:
                            line.append(" -> ")
                                .append(((SymbolicLink)entry.getInode()).target);
                            break;
                        case NAMED_PIPE:
                            line.append('|');
                            break;
                        default:
                            line.append('?');
                    }

                    System.out.println(line);
                }

                System.out.println();
            }
        } catch (VirtualSystemException ex) {
            System.out.printf("ll: %s: %s\n", path, ex.getMessage());
        }
    }

    private static char[] llMode(Inode inode) {
        char[] entry = new char[10];

        switch (inode.type) {
            case REGULAR_FILE:
                entry[0] = '-';
                break;
            case DIRECTORY:
                entry[0] = 'd';
                break;
            case BLOCK_DEVICE:
                entry[0] = 'b';
                break;
            case CHARACTER_DEVICE:
                entry[0] = 'c';
                break;
            case SYMBOLIC_LINK:
                entry[0] = 'l';
                break;
            case NAMED_PIPE:
                entry[0] = 'p';
                break;
            default:
                entry[0] = '?';
        }

        entry[1] = ((inode.getMode() & Inode.OWNER_R) > 0) ? 'r' : '-';
        entry[2] = ((inode.getMode() & Inode.OWNER_W) > 0) ? 'w' : '-';
        entry[3] = ((inode.getMode() & Inode.OWNER_X) > 0) ? 'x' : '-';

        entry[4] = ((inode.getMode() & Inode.GROUP_R) > 0) ? 'r' : '-';
        entry[5] = ((inode.getMode() & Inode.GROUP_W) > 0) ? 'w' : '-';
        entry[6] = ((inode.getMode() & Inode.GROUP_X) > 0) ? 'x' : '-';

        entry[7] = ((inode.getMode() & Inode.OTHER_R) > 0) ? 'r' : '-';
        entry[8] = ((inode.getMode() & Inode.OTHER_W) > 0) ? 'w' : '-';
        entry[9] = ((inode.getMode() & Inode.OTHER_X) > 0) ? 'x' : '-';

        if ((inode.getMode() & Inode.SET_UID) > 0) {
            entry[3] = (entry[3] == 'x') ? 's' : 'S';
        }

        if ((inode.getMode() & Inode.SET_GID) > 0) {
            entry[6] = (entry[6] == 'x') ? 's' : 'S';
        }

        if ((inode.getMode() & Inode.STICKY) > 0) {
            entry[9] = (entry[9] == 'x') ? 't' : 'T';
        }

        return entry;
    }

    public static void mount(Filesystem fs, String[] args) {
        if (args.length != 3) {
            System.out.println("Usage: mount filesystem directory");
            return;
        }

        // TODO: add real mounts system to filesystem

        try {
            String name = args[2];
            DirectoryEntry<?> dirent = fs.resolve(name);
            DirectoryEntry<?> parentDirent = fs.resolve(Filesystem.dirname(name));

            if (dirent == parentDirent) {
                throw new VirtualSystemException(VirtualSystemException.EPERM);
            } else if (parentDirent.getInode().type != InodeType.DIRECTORY) {
                throw new VirtualSystemException(VirtualSystemException.ENOENT);
            }

            Directory parent = (Directory)parentDirent.getInode();
            SubFilesystem newfs;

            switch (args[1]) {
                case "devfs":
                    newfs = new DeviceFilesystem(fs);
                    break;
                default:
                    System.out.printf("mount: Unknown filesystem: %s.\n", args[1]);
                    return;
            }

            if (dirent.getInode().type != InodeType.DIRECTORY) {
                throw new VirtualSystemException(VirtualSystemException.ENOTDIR);
            } else if (!((Directory)dirent.getInode()).isEmpty()) {
                throw new VirtualSystemException(VirtualSystemException.EPERM);
            }

            parent.removeEntry(Filesystem.basename(name));
            fs.mount(newfs, Filesystem.basename(args[2]));
        } catch (VirtualSystemException ex) {
            System.out.printf("mount: %s: %s\n", args[2], ex.getMessage());
        }
    }

    public static void rm(Filesystem fs, String[] args) {
        if (args.length == 1) {
            System.out.println("Usage: rm file...");
            return;
        }

        for (int i = 1; i < args.length; i++) {
            rm(fs, args[i]);
        }
    }

    private static void rm(Filesystem fs, String path) {
        try {
            DirectoryEntry<?> dirent = fs.resolve(path);
            if (dirent.getInode().type == InodeType.DIRECTORY) {
                throw new VirtualSystemException(VirtualSystemException.EISDIR);
            }

            dirent = fs.resolve(Filesystem.dirname(path));
            if (dirent.getInode().type != InodeType.DIRECTORY) {
                throw new VirtualSystemException(VirtualSystemException.ENOENT);
            }

            Directory dir = (Directory)dirent.getInode();
            dir.removeEntry(Filesystem.basename(path));
        } catch (VirtualSystemException ex) {
            System.out.printf("rm: %s: %s\n", path, ex.getMessage());
        }
    }

    public static void rrm(Filesystem fs, String[] args) {
        if (args.length == 1) {
            System.out.println("Usage: rrm file...");
            return;
        }

        for (int i = 1; i < args.length; i++) {
            rrm(fs, args[i]);
        }
    }

    private static void rrm(Filesystem fs, String path) {
        try {
            DirectoryEntry<?> dirent = fs.resolve(Filesystem.dirname(path));
            if (dirent.getInode().type != InodeType.DIRECTORY) {
                throw new VirtualSystemException(VirtualSystemException.ENOENT);
            }

            Directory dir = (Directory)dirent.getInode();
            dir.removeEntry(Filesystem.basename(path));
        } catch (VirtualSystemException ex) {
            System.out.printf("rrm: %s: %s\n", path, ex.getMessage());
        }
    }

    public static void rmdir(Filesystem fs, String[] args) {
        if (args.length == 1) {
            System.out.println("Usage: rmdir directory...");
            return;
        }

        for (int i = 1; i < args.length; i++) {
            rmdir(fs, args[i]);
        }
    }

    private static void rmdir(Filesystem fs, String path) {
        try {
            DirectoryEntry<?> dirent = fs.resolve(path);
            if (dirent.getInode().type != InodeType.DIRECTORY) {
                throw new VirtualSystemException(VirtualSystemException.ENOTDIR);
            }

            Directory dir = (Directory)dirent.getInode();
            if (!dir.isEmpty()) {
                throw new VirtualSystemException(VirtualSystemException.ENOTEMPTY);
            }

            dirent = fs.resolve(Filesystem.dirname(path));
            if (dirent.getInode().type != InodeType.DIRECTORY) {
                throw new VirtualSystemException(VirtualSystemException.ENOENT);
            }

            dir = (Directory)dirent.getInode();
            dir.removeEntry(Filesystem.basename(path));
        } catch (VirtualSystemException ex) {
            System.out.printf("rmdir: %s: %s\n", path, ex.getMessage());
        }

    }

    public static void umount(Filesystem fs, String[] args) {
        if (args.length != 2) {
            System.out.println("Usage: umount directory");
            return;
        }

        // TODO
    }
}
