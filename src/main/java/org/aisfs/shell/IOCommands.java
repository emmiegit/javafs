package org.aisfs.shell;

import org.aisfs.FileHandle;
import org.aisfs.Filesystem;
import org.aisfs.VirtualSystemException;
import org.aisfs.misc.FileMode;

import java.io.UnsupportedEncodingException;
import java.util.Scanner;

/**
 * @author Ammon Smith
 */
public final class IOCommands {
    private IOCommands() {
    }

    public static final int BUFFER_SIZE = 1024;

    public static void cat(Filesystem fs, final String[] args) {
        if (args.length == 1) {
            System.out.println("Usage: cat file...");
            return;
        }

        for (int i = 1; i < args.length; i++) {
            try {
                FileHandle fh = new FileHandle(fs, args[i], FileMode.READ_ONLY);

                byte[] data;
                do {
                    data = fh.read(BUFFER_SIZE);

                    for (int j = 0; j < data.length; j++) {
                        System.out.print((char)data[j]);
                    }
                } while (data.length > 0);
            } catch (VirtualSystemException ex) {
                System.out.printf("cat: %s: %s\n", args[i], ex.getMessage());
            }
        }
    }

    public static void xcat(Filesystem fs, final String[] args) {
        if (args.length == 1) {
            System.out.println("Usage: xcat file...");
            return;
        }

        for (int i = 1; i < args.length; i++) {
            try {
                FileHandle fh = new FileHandle(fs, args[i], FileMode.READ_ONLY);

                byte[] data;
                do {
                    data = fh.read(BUFFER_SIZE);

                    for (int j = 0; j < data.length; j++) {
                        System.out.printf("%02x ", data[j]);

                        if (j % 2 == 1) {
                            System.out.print(' ');
                        }
                    }

                    System.out.println();
                } while (data.length > 0);
            } catch (VirtualSystemException ex) {
                System.out.printf("cat: %s: %s\n", args[i], ex.getMessage());
            }
        }
    }

    private static final Scanner input = new Scanner(System.in);
    public static void write(Filesystem fs, final String[] args) {
        if (args.length == 1) {
            System.out.println("Usage: write file...");
            return;
        }

        System.out.println("Enter lines of data. Finish by entering a line with \"<EOF>\".");
        StringBuilder data = new StringBuilder();
        String line;

        do {
            line = input.nextLine();

            if (!line.equals("<EOF>")) {
                data.append(line)
                    .append('\n');
            }
        } while (!line.equals("<EOF>"));

        byte[] bytes;
        try {
            bytes = data.toString().getBytes("ascii");
        } catch (UnsupportedEncodingException ex) {
            System.out.println("Unable to decode string.");
            return;
        }

        for (int i = 1; i < args.length; i++) {
            write(fs, args[i], bytes);
        }
    }

    private static void write(Filesystem fs, String path, byte[] data) {
        try {
            FileHandle fh = new FileHandle(fs, path, FileMode.WRITE_ONLY);
            fh.write(data);
        } catch (VirtualSystemException ex) {
            System.out.printf("write: %s: %s\n", path, ex.getMessage());
        }
    }
}

