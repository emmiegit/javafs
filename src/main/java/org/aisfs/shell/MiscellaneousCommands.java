package org.aisfs.shell;

import org.aisfs.Filesystem;

/**
 * @author Ammon Smith
 */
public final class MiscellaneousCommands {
    private MiscellaneousCommands() {
    }

    public static void basename(String[] args) {
        if (args.length == 1) {
            System.out.println("Usage: basename file...");
            return;
        }

        for (int i = 1; i < args.length; i++) {
            System.out.println(Filesystem.basename(args[i]));
        }
    }

    public static void dirname(String[] args) {
        if (args.length == 1) {
            System.out.println("Usage: dirname file...");
            return;
        }

        for (int i = 1; i < args.length; i++) {
            System.out.println(Filesystem.dirname(args[i]));
        }
    }

    public static void echo(String[] args) {
        for (int i = 1; i < args.length; i++) {
            System.out.print(args[i]);
            System.out.print(" ");
        }

        System.out.println();
    }

    public static void help(String[] args) {
        System.out.println("Available commands:");
        System.out.println(" basename file...       Strips the directory from a filename.");
        System.out.println(" cat file...            Print the contents of files.");
        System.out.println(" chgrp group file...    Change the group ownership of the given files.");
        System.out.println(" chmod mode file...     Change the permissions and settings of a file.");
        System.out.println(" chown owner file...    Change the ownership of the given files.");
        System.out.println(" chown owner:group file...");
        System.out.println(" creat file...          Creates empty files.");
        System.out.println(" dirname file...        Get the path of the directory in a filename");
        System.out.println(" echo text...           Echo the text back to the user.");
        System.out.println(" exit                   Quit the shell.");
        System.out.println(" help                   Get usage information for utilities.");
        System.out.println(" join path...           Joins the paths together.");
        System.out.println(" ll directory...        List the contents of directories in long format.");
        System.out.println(" ls directory...        List the contents of directories.");
        System.out.println(" mkdir directory...     Creates the specified directories.");
        System.out.println(" mknod file major minor Create a character device.");
        System.out.println(" mknod file size        Create a block device.");
        System.out.println(" rm file...             Remove files.");
        System.out.println(" rrm file...            Recursively remove files and directories.");
        System.out.println(" rmdir directory...     Remove empty directories.");
        System.out.println(" touch file...          Update timestamps, or create the file if it doesn't exist.");
        System.out.println(" write file...          Write some text data to the specified files.");
        System.out.println(" xcat file...           Print the contents of files in hexadecimal.");
    }

    public static void join(String[] args) {
        String[] slice = new String[args.length - 1];
        System.arraycopy(args, 1, slice, 0, slice.length);
        System.out.println(Filesystem.joinPaths(slice));
    }
}
