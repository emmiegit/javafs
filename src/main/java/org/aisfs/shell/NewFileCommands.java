package org.aisfs.shell;

import org.aisfs.Filesystem;
import org.aisfs.VirtualSystemException;
import org.aisfs.inode.*;
import org.aisfs.inode.directory.DirectoryEntry;

/**
 * @author Ammon Smith
 */
public final class NewFileCommands {
    private NewFileCommands() {
    }

    public static void creat(Filesystem fs, String[] args) {
        if (args.length == 1) {
            System.out.println("Usage: creat file...");
            return;
        }

        for (int i = 1; i < args.length; i++) {
            creat(fs, args[i]);
        }
    }

    public static void creat(Filesystem fs, String path) {
        try {
            DirectoryEntry<?> dirent = fs.resolve(Filesystem.dirname(path));

            if (dirent.getInode().type != InodeType.DIRECTORY) {
                throw new VirtualSystemException(VirtualSystemException.ENOENT);
            }

            Directory dir = (Directory)dirent.getInode();
            String filename = Filesystem.basename(path);
            RegularFile file = new RegularFile(dirent.getInode().getFilesystem());

            dir.addEntry(new DirectoryEntry<>(file, filename));
        } catch (VirtualSystemException ex) {
            System.out.printf("creat: %s: %s\n", path, ex.getMessage());
        }
    }

    public static void mkdir(Filesystem fs, String[] args) {
        if (args.length == 1) {
            System.out.println("Usage: mkdir directory...");
            return;
        }

        for (int i = 1; i < args.length; i++) {
            mkdir(fs, args[i]);
        }
    }

    private static void mkdir(Filesystem fs, String path) {
        try {
            DirectoryEntry<?> dirent = fs.resolve(Filesystem.dirname(path));

            if (dirent.getInode().type != InodeType.DIRECTORY) {
                throw new VirtualSystemException(VirtualSystemException.ENOENT);
            }

            DirectoryEntry<Directory> parent = (DirectoryEntry<Directory>)dirent;
            parent.getInode().addEntry(new DirectoryEntry<>(new Directory(dirent.getInode().getFilesystem(), parent), Filesystem.basename(path)));
        } catch (VirtualSystemException ex) {
            System.out.printf("mkdir: %s: %s\n", path, ex.getMessage());
        }
    }

    public static void mknod(Filesystem fs, String[] args) {
        if (args.length == 4) {
            mkCharacterDevice(fs, args);
        } else if (args.length == 3) {
            mkBlockDevice(fs, args);
        } else {
            System.out.println("Usage:");
            System.out.println(" mknod file major minor Create a character device.");
            System.out.println(" mknod file size        Create a block device.");
        }
    }

    private static void mkCharacterDevice(Filesystem fs, String[] args) {
        try {
            byte major, minor;
            try {
                major = Byte.parseByte(args[2]);
            } catch (NumberFormatException ex) {
                System.out.printf("Major field is not an integer or is out of range: %s.\n", args[2]);
                return;
            }

            try {
                minor = Byte.parseByte(args[3]);
            } catch (NumberFormatException ex) {
                System.out.printf("Minor field is not an integer or is out of range: %s.\n", args[3]);
                return;
            }

            if (fs.exists(args[1])) {
                throw new VirtualSystemException(VirtualSystemException.EEXIST);
            }

            DirectoryEntry<?> dirent = fs.resolve(Filesystem.dirname(args[1]));

            if (dirent.getInode().type != InodeType.DIRECTORY) {
                throw new VirtualSystemException(VirtualSystemException.ENOENT);
            }

            Directory dir = (Directory)dirent.getInode();
            CharacterDevice charDevice;

            try {
                charDevice = CharacterDevice.createFromMajorMinor(dirent.getInode().getFilesystem(), major, minor);
            } catch (IllegalArgumentException ex) {
                System.out.println(ex.getMessage());
                return;
            }

            dir.addEntry(new DirectoryEntry<>(charDevice, args[1]));
        } catch (VirtualSystemException ex) {
            System.out.printf("mknod: %s: %s\n", args[1], ex.getMessage());
        }
    }

    private static void mkBlockDevice(Filesystem fs, String[] args) {
        try {
            int size;
            try {
                size = Integer.parseInt(args[2]);

                if (size < 0) {
                    throw new NumberFormatException();
                }
            } catch (NumberFormatException ex) {
                System.out.printf("Size is not an integer or is out of range: %s.\n", args[2]);
                return;
            }

            if (fs.exists(args[1])) {
                throw new VirtualSystemException(VirtualSystemException.EEXIST);
            }

            DirectoryEntry<?> dirent = fs.resolve(Filesystem.dirname(args[1]));

            if (dirent.getInode().type != InodeType.DIRECTORY) {
                throw new VirtualSystemException(VirtualSystemException.ENOENT);
            }

            Directory dir = (Directory)dirent.getInode();
            BlockDevice blockDevice = new BlockDevice(dirent.getInode().getFilesystem(), size);

            dir.addEntry(new DirectoryEntry<>(blockDevice, args[1]));
        } catch (VirtualSystemException ex) {
            System.out.printf("mknod: %s: %s\n", args[1], ex.getMessage());
        }
    }

    public static void touch(Filesystem fs, String[] args) {
        if (args.length == 1) {
            System.out.println("Usage: touch file...");
            return;
        }

        for (int i = 1; i < args.length; i++) {
            try {
                // If file exists, update its timestamps.
                DirectoryEntry<?> dirent = fs.resolve(args[i]);
                long time = System.currentTimeMillis();
                dirent.getInode().setAccessTime(time);
                dirent.getInode().setModifiedTime(time);
                dirent.getInode().setChangeTime(time);
            } catch (VirtualSystemException ex) {
                // If it doesn't exist, create it.
                creat(fs, args[i]);
            }
        }
    }
}
