package org.aisfs;

import org.aisfs.filesystem.SubFilesystem;
import org.aisfs.inode.InodeType;
import org.aisfs.inode.directory.DirectoryEntry;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Ammon Smith
 */
public abstract class Inode<I extends Inode> {
    public static final short MAX_PERM  = 07777;
    public static final short SET_UID   = 04000;
    public static final short SET_GID   = 02000;
    public static final short STICKY    = 01000;
    public static final short OWNER_RWX =  0700;
    public static final short OWNER_R   =  0400;
    public static final short OWNER_W   =  0200;
    public static final short OWNER_X   =  0100;
    public static final short GROUP_RWX =  0070;
    public static final short GROUP_R   =  0040;
    public static final short GROUP_W   =  0020;
    public static final short GROUP_X   =  0010;
    public static final short OTHER_RWX =  0007;
    public static final short OTHER_R   =  0004;
    public static final short OTHER_W   =  0002;
    public static final short OTHER_X   =  0001;

    protected final SubFilesystem fs;
    protected final List<DirectoryEntry<?>> links;
    public final InodeType type;
    private int id;
    private short uid, gid;
    private short mode;
    protected int fileSize;
    protected long ctime, mtime, atime;

    protected Inode(SubFilesystem fs, InodeType type, int fileSize) throws VirtualSystemException {
        this.fs = fs;
        this.id = fs.newInodeId();
        this.type = type;
        this.fileSize = fileSize;
        this.links = new ArrayList<>();

        this.ctime = System.currentTimeMillis();
        this.mtime = ctime;
        this.atime = ctime;

        // temp
        this.mode = 0644;
    }

    public final int write(int position, byte[] bytes) throws VirtualSystemException {
        if (fs.readOnly) {
            throw new VirtualSystemException(VirtualSystemException.EROFS);
        }

        return doWrite(position, bytes);
    }

    public final byte[] read(int position, int byteCount) throws VirtualSystemException {
        return doRead(position, byteCount);
    }

    protected abstract int doWrite(int position, byte[] bytes) throws VirtualSystemException;
    protected abstract byte[] doRead(int position, int byteCount) throws VirtualSystemException;

    public final void setOwner(short uid, short gid) {
         if (uid < 0) {
             throw new IllegalArgumentException("User ID must be a positive number.");
         } else if (gid < 0) {
             throw new IllegalArgumentException("Group ID must be a positive number.");
         }

         updateChangedTime();
         this.uid = uid;
         this.gid = gid;
    }

    public final void setMode(short mode) {
        if (0 > mode || mode > MAX_PERM) {
            throw new IllegalArgumentException("Mode must be a value between 0 and octal 7777.");
        }

        updateChangedTime();
        this.mode = mode;
    }

    public final void setChangeTime(long ctime) {
        this.ctime = ctime;
    }

    public final void setModifiedTime(long mtime) {
        updateChangedTime();
        this.mtime = mtime;
    }

    public final void setAccessTime(long atime) {
        updateChangedTime();
        this.atime = atime;
    }

    public final SubFilesystem getFilesystem() {
        return fs;
    }

    public final int getDeviceId() {
        updateAccessTime();
        return id;
    }

    public final short getUserID() {
        updateAccessTime();
        return uid;
    }

    public final short getGroupID() {
        updateAccessTime();
        return gid;
    }

    public final short getMode() {
        updateAccessTime();
        return mode;
    }

    public final List<DirectoryEntry<?>> getLinks() {
        updateAccessTime();
        return Collections.unmodifiableList(links);
    }

    public final int getLinkCount() {
        updateAccessTime();
        return links.size();
    }

    public final int getFileSize() {
        updateAccessTime();
        return fileSize;
    }

    public final long getChangeTime() {
        updateAccessTime();
        return ctime;
    }

    public final long getModifiedTime() {
        updateAccessTime();
        return mtime;
    }

    public final long getAccessTime() {
        updateAccessTime();
        return atime;
    }

    public void addLink(DirectoryEntry<I> dirent) {
        links.add(dirent);
    }

    protected final void updateChangedTime() {
        ctime = System.currentTimeMillis();
    }

    protected final void updateModifiedTime() {
        mtime = System.currentTimeMillis();
    }

    protected final void updateAccessTime() {
        switch (fs.getAccessOption()) {
            case NORMAL_ACCESS_TIME:
                atime = System.currentTimeMillis();
                break;
            case RELATIVE_ACCESS_TIME:
                atime = Math.max(System.currentTimeMillis(), atime);
                break;
            case NO_ACCESS_TIME:
                break;
        }
    }
}
