package org.aisfs;

import com.sun.istack.internal.NotNull;
import org.aisfs.filesystem.SubFilesystem;
import org.aisfs.inode.Directory;
import org.aisfs.inode.InodeType;
import org.aisfs.inode.directory.DirectoryEntry;
import org.aisfs.misc.MultiChildTree;

import java.util.List;

/**
 * @author Ammon Smith
 */
public class Filesystem {
    public static final String PATH_SEPARATOR = "/";
    public static final int SYMBOLIC_LINK_DEPTH = 50;

    @NotNull
    public static String normalizePath(String pathString) {
        List<String> parts = new Path(pathString).getParts();
        StringBuilder newPath = new StringBuilder(PATH_SEPARATOR);

        for (int i = 0; i < parts.size(); i++) {
            newPath.append(parts.get(i));

            if (i < parts.size() - 1) {
                newPath.append(PATH_SEPARATOR);
            }
        }

        return newPath.toString();
    }

    @NotNull
    public static String joinPaths(CharSequence... paths) {
        StringBuilder rawPath = new StringBuilder();

        for (CharSequence path : paths) {
            rawPath.append(path)
                .append(PATH_SEPARATOR);
        }

        StringBuilder path = new StringBuilder();
        String[] parts = rawPath.toString().split(PATH_SEPARATOR);

        for (int i = 0; i < parts.length; i++) {
            if (parts[i].isEmpty()) {
                continue;
            }

            path.append(parts[i]);

            if (i < parts.length - 1) {
                path.append(PATH_SEPARATOR);
            }
        }

        if (paths.length > 0 && paths[0].toString().startsWith(Filesystem.PATH_SEPARATOR)) {
            path.insert(0, Filesystem.PATH_SEPARATOR);
        }

        return path.toString();
    }

    @NotNull
    public static String dirname(@NotNull String pathString) {
        List<String> parts = new Path(pathString).getParts();
        StringBuilder newPath = new StringBuilder();

        for (int i = 0; i < parts.size() - 1; i++) {
            newPath.append(parts.get(i));

            if (i < parts.size() - 1) {
                newPath.append(PATH_SEPARATOR);
            }
        }

        if (pathString.toString().startsWith(Filesystem.PATH_SEPARATOR)) {
            newPath.insert(0, Filesystem.PATH_SEPARATOR);
        }

        return newPath.toString();
    }

    @NotNull
    public static String basename(@NotNull String pathString) {
        List<String> parts = new Path(pathString).getParts();

        if (parts.isEmpty()) {
            return PATH_SEPARATOR;
        } else {
            return parts.get(parts.size() - 1);
        }
    }

    private final MultiChildTree<String, SubFilesystem> mounts;

    public Filesystem() {
        mounts = new MultiChildTree();
    }

    public DirectoryEntry<Directory> getRoot() throws VirtualSystemException {
        SubFilesystem fs = mounts.getValue(new String[0]);

        if (fs == null) {
            // Root partition isn't mounted!
            throw new VirtualSystemException(VirtualSystemException.ENOENT);
        }

        return fs.getRoot();
    }

    public boolean exists(@NotNull String pathString) {
        Path path = new Path(pathString);

        if (!path.absolute) {
            throw new IllegalArgumentException("Can only resolve absolute paths.");
        }

        DirectoryEntry<?> current;
        try {
            current = getRoot();
        } catch (VirtualSystemException ex) {
            return false;
        }

        for (String part : path.getParts()) {
            if (current.getInode().type == InodeType.DIRECTORY) {
                try {
                    current = ((Directory)current.getInode()).resolve(part);
                } catch (VirtualSystemException ex) {
                    return false;
                }
            } else {
                return false;
            }
        }

        return true;
    }

    @NotNull
    public DirectoryEntry<?> resolveLiterally(@NotNull String pathString) throws VirtualSystemException {
        Path path = new Path(pathString);

        if (!path.absolute) {
            throw new VirtualSystemException(VirtualSystemException.ENOENT);
        }

        DirectoryEntry<?> current = getRoot();
        for (String part : path.getParts()) {
            if (current.getInode().type == InodeType.DIRECTORY) {
                // TODO need to check mounts
                current = ((Directory)current.getInode()).resolve(part);
            } else {
                throw new VirtualSystemException(VirtualSystemException.ENOENT);
            }
        }

        return current;
    }

    @NotNull
    public DirectoryEntry<?> resolve(@NotNull String path) throws VirtualSystemException {
        return resolveLiterally(path).dereference();
    }

    public void mount(@NotNull SubFilesystem fs, @NotNull String pathString) throws VirtualSystemException {
        Path path = new Path(pathString);

        if (!path.absolute) {
            throw new VirtualSystemException(VirtualSystemException.ENOENT);
        }

        mounts.addPair(path.getPartArray(), fs);
    }

    public void unmount(@NotNull String pathString) throws VirtualSystemException {
        // TODO
    }
}
