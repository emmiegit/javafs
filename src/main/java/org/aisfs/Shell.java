package org.aisfs;

import org.aisfs.filesystem.StandardFilesystem;
import org.aisfs.shell.FilesystemCommands;
import org.aisfs.shell.IOCommands;
import org.aisfs.shell.NewFileCommands;
import org.aisfs.shell.MiscellaneousCommands;
import org.aisfs.system.Kernel;

import java.util.NoSuchElementException;
import java.util.Scanner;

/**
 * @author Ammon Smith
 */
public final class Shell {
    private static final Scanner input = new Scanner(System.in);

    public static void main(final String[] args) {
        Filesystem fs = Kernel.getInstance().filesystem;
        try {
            fs.mount(new StandardFilesystem(fs), "/");
        } catch (VirtualSystemException ex) {
            System.out.printf("Unable to mount /: %s.\n", ex.getMessage());
        }

        String[] command;
        do {
            System.out.print("$ ");
            try {
                command = input.nextLine().trim().split(" ");

                if (command.length == 0 || command[0].isEmpty() || command[0].charAt(0) == '#') {
                    continue;
                }
            } catch (NoSuchElementException ex) {
                break;
            }

            if (command.length == 0 || command[0].isEmpty()) {
                continue;
            }

            switch (command[0]) {
                case "basename":
                    MiscellaneousCommands.basename(command);
                    break;
                case "cat":
                    IOCommands.cat(fs, command);
                    break;
                case "chgrp":
                    FilesystemCommands.chgrp(fs, command);
                    break;
                case "chmod":
                    FilesystemCommands.chmod(fs, command);
                    break;
                case "chown":
                    FilesystemCommands.chown(fs, command);
                    break;
                case "creat":
                    NewFileCommands.creat(fs, command);
                    break;
                case "dirname":
                    MiscellaneousCommands.dirname(command);
                    break;
                case "echo":
                    MiscellaneousCommands.echo(command);
                    break;
                case "exit":
                    System.exit(0);
                    break;
                case "help":
                    MiscellaneousCommands.help(command);
                    break;
                case "mkdir":
                    NewFileCommands.mkdir(fs, command);
                    break;
                case "mknod":
                    NewFileCommands.mknod(fs, command);
                    break;
                case "mount":
                    FilesystemCommands.mount(fs, command);
                    break;
                case "join":
                    MiscellaneousCommands.join(command);
                    break;
                case "ls":
                    FilesystemCommands.ls(fs, command);
                    break;
                case "ll":
                    FilesystemCommands.ll(fs, command);
                    break;
                case "rm":
                    FilesystemCommands.rm(fs, command);
                    break;
                case "rrm":
                    FilesystemCommands.rrm(fs, command);
                    break;
                case "rmdir":
                    FilesystemCommands.rmdir(fs, command);
                    break;
                case "touch":
                    NewFileCommands.touch(fs, command);
                    break;
                case "umount":
                    FilesystemCommands.umount(fs, command);
                    break;
                case "write":
                    IOCommands.write(fs, command);
                    break;
                case "xcat":
                    IOCommands.xcat(fs, command);
                    break;
                default:
                    System.out.printf("Unknown program: %s\n", command[0]);
            }
        } while (true);
    }
}
