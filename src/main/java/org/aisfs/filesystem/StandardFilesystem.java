package org.aisfs.filesystem;

import com.sun.istack.internal.NotNull;

import org.aisfs.Filesystem;
import org.aisfs.VirtualSystemException;
import org.aisfs.misc.AccessTimeOptions;

/**
 * @author Ammon Smith
 */
public class StandardFilesystem extends SubFilesystem {
    public StandardFilesystem(@NotNull Filesystem fs, @NotNull AccessTimeOptions accessOption, int maxInodeId) throws VirtualSystemException {
        super(fs, "stdfs", accessOption, maxInodeId);
    }

    public StandardFilesystem(@NotNull Filesystem fs, @NotNull AccessTimeOptions accessOption) throws VirtualSystemException {
        super(fs, "stdfs", accessOption);
    }

    public StandardFilesystem(@NotNull Filesystem fs, int maxInodeId) throws VirtualSystemException {
        super(fs, "stdfs", maxInodeId);
    }

    public StandardFilesystem(Filesystem fs) throws VirtualSystemException {
        super(fs, "stdfs");
    }
}
