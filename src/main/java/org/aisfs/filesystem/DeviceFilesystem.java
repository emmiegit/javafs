package org.aisfs.filesystem;


import org.aisfs.Filesystem;
import org.aisfs.VirtualSystemException;
import org.aisfs.inode.BlockDevice;
import org.aisfs.inode.device.*;
import org.aisfs.inode.directory.DirectoryEntry;

/**
 * @author Ammon Smith
 */
public class DeviceFilesystem extends SubFilesystem {
    public DeviceFilesystem(Filesystem fs) throws VirtualSystemException {
        super(fs, "devfs");

        try {
            getRoot().getInode().addEntry(new DirectoryEntry<>(new Null(this), "null"));
            getRoot().getInode().addEntry(new DirectoryEntry<>(new Full(this), "full"));
            getRoot().getInode().addEntry(new DirectoryEntry<>(new Random(this), "random"));
            getRoot().getInode().addEntry(new DirectoryEntry<>(new Zero(this), "zero"));
            getRoot().getInode().addEntry(new DirectoryEntry<>(new SystemJournalDevice(this), "kmsg"));

            BlockDevice hdd = new BlockDevice(this, 500);
            hdd.setMode((short)0660);
            getRoot().getInode().addEntry(new DirectoryEntry<>(hdd, "sda"));
        } catch (VirtualSystemException ex) {
            throw new VirtualSystemException(VirtualSystemException.EINTR);
        }
    }
}
