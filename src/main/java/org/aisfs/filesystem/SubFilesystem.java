package org.aisfs.filesystem;

import com.sun.istack.internal.NotNull;
import org.aisfs.Filesystem;
import org.aisfs.VirtualSystemException;
import org.aisfs.inode.Directory;
import org.aisfs.inode.directory.DirectoryEntry;
import org.aisfs.misc.AccessTimeOptions;

/**
 * @author Ammon Smith
 */
public abstract class SubFilesystem {
    public final String name;
    public boolean readOnly;
    protected final Filesystem fs;
    protected final DirectoryEntry<Directory> root;
    protected AccessTimeOptions accessOption;
    private int nextInodeId = 0;
    public final int maxInodeId;

    protected SubFilesystem(@NotNull Filesystem fs, @NotNull String name, @NotNull AccessTimeOptions accessOption, int maxInodeId) throws VirtualSystemException {
        this.name = name;
        this.fs = fs;
        this.root = new DirectoryEntry<>(new Directory(this, null), Filesystem.PATH_SEPARATOR);

        if (readOnly) {
            this.accessOption = AccessTimeOptions.NO_ACCESS_TIME;
        } else {
            this.accessOption = accessOption;
        }

        if (maxInodeId <= 0) {
            throw new IllegalArgumentException("Maximum inode id is negative or zero.");
        } else {
            this.maxInodeId = maxInodeId;
        }
    }

    protected SubFilesystem(@NotNull Filesystem fs, @NotNull String name, @NotNull AccessTimeOptions accessOption) throws VirtualSystemException {
        this(fs, name, accessOption, Integer.MAX_VALUE);
    }

    protected SubFilesystem(@NotNull Filesystem fs, @NotNull String name, int maxInodeId) throws VirtualSystemException {
        this(fs, name, AccessTimeOptions.NORMAL_ACCESS_TIME, maxInodeId);
    }

    protected SubFilesystem(Filesystem fs, @NotNull String name) throws VirtualSystemException {
        this(fs, name, AccessTimeOptions.NORMAL_ACCESS_TIME, Integer.MAX_VALUE);
    }

    public Filesystem getMasterFilesystem() {
        return fs;
    }

    public DirectoryEntry<Directory> getRoot() {
        return root;
    }

    @NotNull
    public AccessTimeOptions getAccessOption() {
        return accessOption;
    }

    public int newInodeId() throws VirtualSystemException {
        nextInodeId++;

        if (nextInodeId < 0) {
            throw new VirtualSystemException(VirtualSystemException.ENOSPC);
        }

        return nextInodeId;
    }
}
