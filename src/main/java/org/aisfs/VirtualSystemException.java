package org.aisfs;

/**
 * @author Ammon Smith
 */
public class VirtualSystemException extends Exception {
    public static final int EPERM = 1;
    public static final int ENOENT = 2;
    public static final int EINTR = 4;
    public static final int EIO = 5;
    public static final int EBADF = 9;
    public static final int EAGAIN = 11;
    public static final int EACCES = 13;
    public static final int ENOTBLK = 15;
    public static final int EBUSY = 16;
    public static final int EEXIST = 17;
    public static final int EXDEV = 18;
    public static final int ENODEV = 19;
    public static final int ENOTDIR = 20;
    public static final int EISDIR = 21;
    public static final int EINVAL = 22;
    public static final int ENFILE = 23;
    public static final int EMFILE = 24;
    public static final int ENOTTY = 25;
    public static final int ETXTBSY = 26;
    public static final int EFBIG = 27;
    public static final int ENOSPC = 28;
    public static final int ESPIPE = 29;
    public static final int EROFS = 30;
    public static final int EPIPE = 32;
    public static final int EDOM = 33;
    public static final int ERANGE = 34;
    public static final int ENOSYS = 38;
    public static final int ENOTEMPTY = 39;
    public static final int ELOOP = 40;

    public static String getMessage(int errno) {
        switch (errno) {
            case EPERM:
                return "Operation not permitted";
            case ENOENT:
                return "No such file or directory";
            case EINTR:
                return "Interrupted system call";
            case EIO:
                return "I/O error";
            case EBADF:
                return "Bad file descriptor";
            case EAGAIN:
                return "Try again";
            case EACCES:
                return "Permission denied";
            case ENOTBLK:
                return "Block device required";
            case EBUSY:
                return "Device or resource busy";
            case EEXIST:
                return "File exists";
            case EXDEV:
                return "Cross-device link";
            case ENODEV:
                return "No such device";
            case ENOTDIR:
                return "Not a directory";
            case EISDIR:
                return "Is a directory";
            case EINVAL:
                return "Invalid argument";
            case ENFILE:
                return "Too many open files in system";
            case EMFILE:
                return "Too many open files";
            case ENOTTY:
                return "Not a typewriter";
            case ETXTBSY:
                return "Text file busy";
            case EFBIG:
                return "File too large";
            case ENOSPC:
                return "No space left on device";
            case ESPIPE:
                return "Illegal seek";
            case EROFS:
                return "Read-only file system";
            case EPIPE:
                return "Broken pipe";
            case EDOM:
                return "Numerical argument out of domain";
            case ERANGE:
                return "Numerical result out of range";
            case ENOSYS:
                return "Function not implemented";
            case ENOTEMPTY:
                return "Directory not empty";
            case ELOOP:
                return "Too many levels of symbolic links";
            default:
                throw new IllegalArgumentException("Invalid errno: " + errno);
        }
    }

    public final int errno;

    public VirtualSystemException(int errno) {
        super(getMessage(errno));
        this.errno = errno;
    }

    public VirtualSystemException(Throwable cause, int errno) {
        super(getMessage(errno), cause);
        this.errno = errno;
    }
}
