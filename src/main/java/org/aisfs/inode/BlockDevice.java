package org.aisfs.inode;

import org.aisfs.VirtualSystemException;
import org.aisfs.Inode;
import org.aisfs.filesystem.SubFilesystem;

/**
 * @author Ammon Smith
 */
public class BlockDevice extends Inode<BlockDevice> {
    private final byte[] data;

    public BlockDevice(SubFilesystem fs, int byteCount) throws VirtualSystemException {
        super(fs, InodeType.BLOCK_DEVICE, byteCount);
        data = new byte[byteCount];
    }

    @Override
    public int doWrite(int position, byte[] bytes) throws VirtualSystemException {
        updateModifiedTime();

        if (position + bytes.length > data.length) {
            throw new VirtualSystemException(VirtualSystemException.ENOSPC);
        }

        System.arraycopy(bytes, 0, data, position, bytes.length);
        return bytes.length;
    }

    @Override
    protected byte[] doRead(int position, int byteCount) throws VirtualSystemException {
        updateAccessTime();

        byte[] copy = new byte[Math.min(data.length - position, byteCount)];
        System.arraycopy(data, position, copy, 0, copy.length);
        return copy;
    }
}
