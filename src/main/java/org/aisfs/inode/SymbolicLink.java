package org.aisfs.inode;

import org.aisfs.VirtualSystemException;
import org.aisfs.Inode;
import org.aisfs.filesystem.SubFilesystem;

/**
 * @author Ammon Smith
 */
public class SymbolicLink extends Inode {
    public final String target;

    public SymbolicLink(SubFilesystem fs, String target) throws VirtualSystemException {
        super(fs, InodeType.SYMBOLIC_LINK, target.length());
        this.target = target;
    }

    @Override
    protected int doWrite(int position, byte[] bytes) throws VirtualSystemException {
        // TODO
        return -1;
    }

    @Override
    protected byte[] doRead(int position, int byteCount) throws VirtualSystemException {
        // TODO
        return null;
    }
}
