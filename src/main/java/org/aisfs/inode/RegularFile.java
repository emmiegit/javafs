package org.aisfs.inode;

import org.aisfs.VirtualSystemException;
import org.aisfs.Inode;
import org.aisfs.filesystem.SubFilesystem;
import org.aisfs.misc.DynamicByteArray;

/**
 * @author Ammon Smith
 */
public class RegularFile extends Inode {
    private final DynamicByteArray data;

    public RegularFile(SubFilesystem fs) throws VirtualSystemException {
        super(fs, InodeType.REGULAR_FILE, 0);
        data = new DynamicByteArray();
    }

    @Override
    protected int doWrite(int position, byte[] bytes) throws VirtualSystemException {
        updateModifiedTime();
        data.set(position, bytes);
        fileSize = data.getSize();
        return bytes.length;
    }

    @Override
    protected byte[] doRead(int position, int byteCount) throws VirtualSystemException {
        updateAccessTime();
        return data.get(position, byteCount);
    }

    public void truncate() {
        data.clear();
    }
}
