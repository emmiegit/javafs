package org.aisfs.inode;

/**
 * @author Ammon Smith
 */
public enum InodeType {
    REGULAR_FILE,
    DIRECTORY,
    BLOCK_DEVICE,
    CHARACTER_DEVICE,
    SYMBOLIC_LINK,
    NAMED_PIPE,
}
