package org.aisfs.inode.directory;

import org.aisfs.Filesystem;
import org.aisfs.VirtualSystemException;
import org.aisfs.Inode;
import org.aisfs.inode.InodeType;
import org.aisfs.inode.SymbolicLink;

/**
 * @author Ammon Smith
 */
public class DirectoryEntry<I extends Inode> {
    private final I inode;
    private String name;

    public DirectoryEntry(I inode, String name) {
        if (inode == null) {
            throw new IllegalArgumentException("Inode cannot be null.");
        } else if (name == null || name.isEmpty()) {
            throw new IllegalArgumentException("Name cannot be null or empty.");
        } else if (name.contains("\0")) {
            throw new IllegalArgumentException("Name cannot contain null characters.");
        }

        this.inode = inode;
        this.name = Filesystem.basename(name);
        inode.addLink(this);
    }

    public DirectoryEntry<?> dereference() throws VirtualSystemException {
        if (inode.type == InodeType.SYMBOLIC_LINK) {
            return dereference(0);
        } else {
            return this;
        }
    }

    private DirectoryEntry<?> dereference(int attempts) throws VirtualSystemException {
        SymbolicLink symlink = (SymbolicLink)inode;

        if (attempts > Filesystem.SYMBOLIC_LINK_DEPTH) {
            throw new VirtualSystemException(VirtualSystemException.ELOOP);
        } else {
            DirectoryEntry<?> destination;
            if (symlink.target.startsWith(Filesystem.PATH_SEPARATOR)) {
                destination = symlink.getFilesystem().getMasterFilesystem().resolve(symlink.target);
            } else {
                destination = symlink.getFilesystem().getMasterFilesystem().resolve(Filesystem.joinPaths(name, symlink.target));
            }

            if (destination.getInode().type == InodeType.SYMBOLIC_LINK) {
                return destination.dereference(attempts + 1);
            }

            throw new VirtualSystemException(VirtualSystemException.ENOENT);
        }
    }

    public void rename(String name) {
        this.name = name;
    }

    public I getInode() {
        return inode;
    }

    public String getName() {
        return name;
    }
}
