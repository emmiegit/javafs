package org.aisfs.inode.device;

import org.aisfs.VirtualSystemException;
import org.aisfs.filesystem.SubFilesystem;
import org.aisfs.inode.CharacterDevice;

/**
 * @author Ammon Smith
 */
public class Full extends CharacterDevice {
    public Full(SubFilesystem fs) throws VirtualSystemException {
        super(fs, 1, 7);
        setMode((short)0666);
    }

    @Override
    protected int doWrite(int position, byte[] bytes) throws VirtualSystemException {
        updateModifiedTime();
        throw new VirtualSystemException(VirtualSystemException.ENOSPC);
    }

    @Override
    protected byte[] doRead(int position, int byteCount) throws VirtualSystemException {
        updateAccessTime();
        return new byte[byteCount];
    }
}
