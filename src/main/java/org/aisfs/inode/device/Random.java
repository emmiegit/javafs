package org.aisfs.inode.device;

import org.aisfs.VirtualSystemException;
import org.aisfs.filesystem.SubFilesystem;
import org.aisfs.inode.CharacterDevice;

/**
 * @author Ammon Smith
 */
public class Random extends CharacterDevice {
    private static final java.util.Random rand;

    static {
        rand = new java.util.Random();
    }

    public Random(SubFilesystem fs) throws VirtualSystemException {
        super(fs, 1, 8);
        setMode((short)0666);
    }

    @Override
    protected int doWrite(int position, byte[] bytes) {
        updateModifiedTime();
        return bytes.length;
    }

    @Override
    protected byte[] doRead(int position, int byteCount) {
        updateAccessTime();
        byte[] bytes = new byte[byteCount];
        rand.nextBytes(bytes);
        return bytes;
    }
}

