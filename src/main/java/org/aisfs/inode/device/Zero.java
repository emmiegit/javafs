package org.aisfs.inode.device;

import org.aisfs.VirtualSystemException;
import org.aisfs.filesystem.SubFilesystem;
import org.aisfs.inode.CharacterDevice;

/**
 * @author Ammon Smith
 */
public class Zero extends CharacterDevice {
    public Zero(SubFilesystem fs) throws VirtualSystemException {
        super(fs, 1, 5);
        setMode((short)0666);
    }

    @Override
    protected int doWrite(int position, byte[] bytes) {
        updateModifiedTime();
        return bytes.length;
    }

    @Override
    protected byte[] doRead(int position, int byteCount) {
        updateAccessTime();
        return new byte[byteCount];
    }
}
