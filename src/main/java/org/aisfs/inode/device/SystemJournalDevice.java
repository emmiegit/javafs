package org.aisfs.inode.device;

import org.aisfs.VirtualSystemException;
import org.aisfs.filesystem.SubFilesystem;
import org.aisfs.inode.CharacterDevice;
import org.aisfs.system.Kernel;

/**
 * @author Ammon Smith
 */
public class SystemJournalDevice extends CharacterDevice {
    public SystemJournalDevice(SubFilesystem fs) throws VirtualSystemException {
        super(fs, 1, 5);
        setMode((short)0644);
    }

    @Override
    protected int doWrite(int position, byte[] bytes) {
        String message = new String(bytes);
        Kernel.getInstance().journal.log(message);
        updateModifiedTime();
        return bytes.length;
    }

    @Override
    protected byte[] doRead(int position, int byteCount) {
        byte[] fullBytes = Kernel.getInstance().journal.getFullJournal().getBytes();
        byte[] readBytes = new byte[byteCount];
        System.arraycopy(fullBytes, position, readBytes, 0, byteCount);
        updateAccessTime();
        return new byte[byteCount];
    }
}
