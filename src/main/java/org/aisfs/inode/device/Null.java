package org.aisfs.inode.device;

import org.aisfs.VirtualSystemException;
import org.aisfs.filesystem.SubFilesystem;
import org.aisfs.inode.CharacterDevice;

/**
 * @author Ammon Smith
 */
public class Null extends CharacterDevice {
    public Null(SubFilesystem fs) throws VirtualSystemException {
        super(fs, 1, 3);
        setMode((short)0666);
    }

    @Override
    protected int doWrite(int position, byte[] bytes) throws VirtualSystemException {
        updateModifiedTime();
        return bytes.length;
    }

    @Override
    protected byte[] doRead(int position, int byteCount) throws VirtualSystemException {
        updateAccessTime();
        return new byte[0];
    }
}
