package org.aisfs.inode;

import org.aisfs.VirtualSystemException;
import org.aisfs.Inode;
import org.aisfs.filesystem.SubFilesystem;
import org.aisfs.inode.directory.DirectoryEntry;

/**
 * @author Ammon Smith
 */
public class Directory extends Inode {
    private final boolean root;

    public Directory(SubFilesystem fs, DirectoryEntry<Directory> parent) throws VirtualSystemException {
        super(fs, InodeType.DIRECTORY, 0);

        Directory parentDir;
        if (parent == null) {
            root = true;
            parentDir = this;
        } else {
            root = false;
            parentDir = parent.getInode();
        }

        links.add(new DirectoryEntry<>(this, "."));
        links.add(new DirectoryEntry<>(parentDir, ".."));

        setMode((short)0755);
    }

    public void addEntry(DirectoryEntry<?> dirent) throws VirtualSystemException {
        for (Object entry : links) {
            if (((DirectoryEntry<?>)entry).getName().equals(dirent.getName())) {
                throw new VirtualSystemException(VirtualSystemException.EEXIST);
            }
        }

        links.add(dirent);
    }

    public void removeEntry(String name) throws VirtualSystemException {
        if (name.equals(".") || name.equals("..")) {
            throw new VirtualSystemException(VirtualSystemException.EPERM);
        }

        for (Object entry : links) {
            if (((DirectoryEntry<?>)entry).getName().equals(name)) {
                links.remove(entry);
                return;
            }
        }

        throw new VirtualSystemException(VirtualSystemException.ENOENT);
    }

    @Override
    protected int doWrite(int position, byte[] bytes) throws VirtualSystemException {
        throw new VirtualSystemException(VirtualSystemException.EISDIR);
    }

    @Override
    protected byte[] doRead(int position, int byteCount) throws VirtualSystemException {
        throw new VirtualSystemException(VirtualSystemException.EISDIR);
    }

    public boolean isRoot() {
        return root;
    }

    public boolean isEmpty() {
        return links.size() == 2;
    }

    public boolean has(String name) {
        updateAccessTime();

        for (Object dirent : links) {
            if (((DirectoryEntry<?>)dirent).getName().equals(name)) {
                return true;
            }
        }

        return false;
    }

    public DirectoryEntry<?> resolve(String name) throws VirtualSystemException {
        updateAccessTime();

        for (Object item : links) {
            DirectoryEntry<?> dirent = (DirectoryEntry<?>)item;
            if (dirent.getName().equals(name)) {
                return dirent;
            }
        }

        throw new VirtualSystemException(VirtualSystemException.ENOENT);
    }
}
