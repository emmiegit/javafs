package org.aisfs.inode;

import org.aisfs.VirtualSystemException;
import org.aisfs.Inode;
import org.aisfs.filesystem.SubFilesystem;
import org.aisfs.inode.device.Full;
import org.aisfs.inode.device.Null;
import org.aisfs.inode.device.Random;
import org.aisfs.inode.device.Zero;

/**
 * @author Ammon Smith
 */
public abstract class CharacterDevice extends Inode<CharacterDevice> {
    public static CharacterDevice createFromMajorMinor(SubFilesystem fs, byte major, byte minor) throws VirtualSystemException {
        switch (major) {
            case 1:
                switch (minor) {
                    case 3:
                        return new Null(fs);
                    case 5:
                        return new Zero(fs);
                    case 7:
                        return new Full(fs);
                    case 8:
                        return new Random(fs);
                    default:
                        throw new IllegalArgumentException(String.format("Invalid major/minor: %d, %d.", major, minor));
                }
            default:
                throw new IllegalArgumentException(String.format("Invalid major/minor: %d, %d.", major, minor));
        }
    }

    protected final byte major, minor;

    protected CharacterDevice(SubFilesystem fs, int major, int minor) throws VirtualSystemException {
        super(fs, InodeType.CHARACTER_DEVICE, 0);

        if (0 > major || major > Byte.MAX_VALUE) {
            throw new IllegalArgumentException("Major value out of range: " + major);
        } else if (0 > minor || minor > Byte.MAX_VALUE) {
            throw new IllegalArgumentException("Minor value out of range: " + minor);
        }

        this.major = (byte)major;
        this.minor = (byte)minor;
    }

    public int getMajor() {
        return major;
    }

    public int getMinor() {
        return minor;
    }
}
