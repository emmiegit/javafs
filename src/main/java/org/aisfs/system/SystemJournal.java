package org.aisfs.system;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * @author Ammon Smith
 */
public class SystemJournal {
    private final StringBuffer journal;

    public SystemJournal() {
        journal = new StringBuffer();
    }

    public void log(String message) {
        for (String line : message.split("\n")) {
            journal.append(String.format("%s: %s\n", new Date(), line));
        }
    }

    public String getFullJournal() {
        return journal.toString();
    }
}
