package org.aisfs.system;

import org.aisfs.Filesystem;
import org.aisfs.Path;
import org.aisfs.VirtualSystemException;
import org.aisfs.Inode;
import org.aisfs.inode.InodeType;

/**
 * @author Ammon Smith
 */
public class UserManager {
    public static final String USERS_FILE = "/etc/passwd";
    public static final String GROUPS_FILE = "/etc/group";

    private final Filesystem fs;

    public UserManager(Filesystem fs) {
        this.fs = fs;
    }

    public UserEntry getUserEntry(int uid) throws VirtualSystemException{
        Inode inode = fs.resolve(USERS_FILE).getInode();

        if (inode.type != InodeType.DIRECTORY) {
            String fullFile = new String(inode.read(0, inode.getFileSize()));

            /*
                Format:
                (username):(password-hash):(user-id):(group-id):(home-directory)
             */
            for (String entry : fullFile.split("\n")) {
                String[] fields = entry.split(":");

                if (fields.length < 5) {
                    continue;
                }

                int thisUid;
                try {
                    thisUid = Integer.parseInt(fields[2]);
                } catch (NumberFormatException ex) {
                    continue;
                }

                if (thisUid == uid) {
                    int thisGid;
                    try {
                        thisGid = Integer.parseInt(fields[3]);
                    } catch (NumberFormatException ex) {
                        thisGid = -1;
                    }

                    Path path;
                    try {
                        path = new Path(fields[4]);
                    } catch (IllegalArgumentException ex) {
                        path = null;
                    }

                    return new UserEntry(fields[0], fields[1], thisUid, thisGid, path);
                }
            }

            return null;
        } else {
            throw new VirtualSystemException(VirtualSystemException.EISDIR);
        }
    }

    public String getUserName(int uid) throws VirtualSystemException {
        return getUserEntry(uid).userName;
    }

    public String safeGetUserName(int uid) {
        try {
            return getUserName(uid);
        } catch (VirtualSystemException ex) {
            return Integer.toString(uid);
        }
    }

    public static final class UserEntry {
        public final String userName;
        public final String passwordHash;
        public final int userId;
        public final int groupId;
        public final Path homeDirectory;

        public UserEntry(String userName, String passwordHash, int userId, int groupId, Path homeDirectory) {
            this.userName = userName;
            this.passwordHash = passwordHash;
            this.userId = userId;
            this.groupId = groupId;
            this.homeDirectory = homeDirectory;
        }
    }

    public GroupEntry getGroupEntry(int gid) throws VirtualSystemException {
        Inode inode = fs.resolve(GROUPS_FILE).getInode();

        if (inode.type != InodeType.DIRECTORY) {
            String fullFile = new String(inode.read(0, inode.getFileSize()));

            /*
                Format:
                (group-name):(password-hash):(group-id):(user1-in-group,user2,user3...)
             */
            for (String entry : fullFile.split("\n")) {
                String[] fields = entry.split(":");

                if (fields.length < 4) {
                    continue;
                }

                int thisGid;
                try {
                    thisGid = Integer.parseInt(fields[1]);
                } catch (NumberFormatException ex) {
                    continue;
                }

                if (thisGid == gid) {
                    return new GroupEntry(fields[0], fields[1], thisGid, fields[2]);
                }
            }

            return null;
        } else {
            throw new VirtualSystemException(VirtualSystemException.EISDIR);
        }
    }

    public String getGroupName(int gid) throws VirtualSystemException {
        return getGroupEntry(gid).groupName;
    }

    public String safeGetGroupName(int gid) {
        try {
            return getGroupName(gid);
        } catch (VirtualSystemException ex) {
            return Integer.toString(gid);
        }
    }
    public static final class GroupEntry {
        public final String groupName;
        public final String passwordHash;
        public final int groupId;
        private final String[] usersInGroup;

        public GroupEntry(String groupName, String passwordHash, int groupId, String usersInGroup) {
            this.groupName = groupName;
            this.passwordHash = passwordHash;
            this.groupId = groupId;
            this.usersInGroup = usersInGroup.split(",");
        }

        public String[] getUsersInGroup() {
            return usersInGroup.clone();
        }
    }
}
