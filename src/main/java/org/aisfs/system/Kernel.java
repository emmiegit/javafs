package org.aisfs.system;

import org.aisfs.Filesystem;

/**
 * @author Ammon Smith
 */
public class Kernel {
    private static Kernel instance;

    public static Kernel getInstance() {
        if (instance == null) {
            instance = new Kernel();
        }

        return instance;
    }

    public final Filesystem filesystem;
    public final UserManager userManager;
    public final SystemJournal journal;

    private Kernel() {
        filesystem = new Filesystem();
        userManager = new UserManager(filesystem);
        journal = new SystemJournal();
    }
}
